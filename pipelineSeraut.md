# scRNA test

This is my practice of scRNA 

## Getting started
data was downloaded from illumina 10x data 
R session 4.1 
tools Seraut dplyr 

```R

pbmc.data<-Read10X(data.dir="/project/meissner_training/qiongyu/data/seraut/filtered_gene_bc_matrices/hg19/")
dim(pbmc.data)
[1] 32738  2700  #32738 genes 2700 cells?
pbmc.data[c("OR4F5", "FAM138A", "MS4A1"), 1:30]
3 x 30 sparse Matrix of class "dgCMatrix"
   [[ suppressing 30 column names ‘AAACATACAACCAC-1’, ‘AAACATTGAGCTAC-1’, ‘AAACATTGATCAGC-1’ ... ]]

OR4F5   . . . . . . . . . . . . . . . . . . . .  . . . . . . . . . .
FAM138A . . . . . . . . . . . . . . . . . . . .  . . . . . . . . . .
MS4A1   . 6 . . . . . . 1 1 1 . . . . . . . . . 36 1 2 . . 2 . . . 。

# check how many gene expression situation in each cell
at_least_one <- apply(pbmc.data, 2, function(x) sum(x>0))
 hist(at_least_one,breaks = 100)
dev.off()

# create seurat object
pbmc <- CreateSeuratObject(counts = pbmc.data, project = "pbmc3k", min.cells = 3, min.features = 200)
pbmc
An object of class Seurat
13714 features across 2700 samples within 1 assay
Active assay: RNA (13714 features, 0 variable feature

# add metadata of mitochrondira .have the view of pbmc metadata
pbmc[["percent.mt"]] <- PercentageFeatureSet(pbmc, pattern = "^MT-")
head(pbmc@meta.data, 5)
                 orig.ident nCount_RNA nFeature_RNA percent.mt
AAACATACAACCAC-1     pbmc3k       2419          779  3.0177759
AAACATTGAGCTAC-1     pbmc3k       4903         1352  3.7935958
AAACATTGATCAGC-1     pbmc3k       3147         1129  0.8897363
AAACCGTGCTTCCG-1     pbmc3k       2639          960  1.7430845
AAACCGTGTATGCG-1     pbmc3k        980          521  1.2244898

# nCount = unique reads  nFeature= genes
pdf("/project/meissner_training/qiongyu/data/seraut/feature_counts_distribution.pdf")
VlnPlot(pbmc, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)
plot1 <- FeatureScatter(pbmc, feature1 = "nCount_RNA", feature2 = "percent.mt")
plot2 <- FeatureScatter(pbmc, feature1 = "nCount_RNA", feature2 = "nFeature_RNA")
dev.off()

#subet pbmc by nfeature and percent.mt
pbmc <- subset(pbmc, subset = nFeature_RNA > 200 & nFeature_RNA < 2500 & percent.mt < 5)

# normalization
# questions scale fatocrs 10000
pbmc <- NormalizeData(pbmc, normalization.method = "LogNormalize", scale.factor = 10000)

# find toppest variable .. plot them label the first 10 most variant
pbmc <- FindVariableFeatures(pbmc, selection.method = "vst", nfeatures = 2000)
# top 2000 of differential genes be picked
 head(VariableFeatures(pbmc))
[1] "PPBP"   "LYZ"    "S100A9" "IGLL5"  "GNLY"   "FTL"
top10 <- head(VariableFeatures(pbmc), 10)
pdf("/project/meissner_training/qiongyu/data/seraut/variableplot.pdf")
plot1<-VariableFeaturePlot(pbmc)
plot2<-LabelPoints(plot=plot1,points=top10,repel=T)
dev.off()

# ScaleData  the results woulbe in pbmc[["RNA"]]@scale.data
# scale all the genes feature except the vars.to.regress ;
# except those you don't wanna select
all.genes <- rownames(pbmc)
pbmc <- ScaleData(pbmc, features = all.genes)
pbmc <- ScaleData(pbmc, vars.to.regress = "percent.mt")

#PCA
pbmc <- RunPCA(pbmc, features = VariableFeatures(object = pbmc))
> print(pbmc[["pca"]], dims = 1:5, nfeatures = 7)
PC_ 1
Positive:  CST3, TYROBP, LST1, AIF1, FTL, FTH1, LYZ
Negative:  MALAT1, LTB, IL32, IL7R, CD2, B2M, ACAP1
PC_ 2
Positive:  CD79A, MS4A1, TCL1A, HLA-DQA1, HLA-DQB1, HLA-DRA, LINC00926
Negative:  VIM, IL7R, S100A6, S100A8, IL32, S100A4, GIMAP7
PC_ 5
Positive:  GZMB, FGFBP2, S100A8, NKG7, GNLY, CCL4, PRF1
Negative:  LTB, IL7R, CKB, MS4A7, RP11-290F20.3, AQP3, SIGLEC10

pdf("/project/meissner_training/qiongyu/data/seraut/pca.pdf")
> VizDimLoadings(pbmc, dims = 1:2, reduction = "pca")
> DimPlot(pbmc, reduction = "pca",split.by = 'ident')
> dev.off()
pdf
  2
#heatmap
> pdf("/project/meissner_training/qiongyu/data/seraut/pcaheat.pdf")
> DimHeatmap(pbmc, dims = 1:2, cells = 500, balanced = TRUE)
> dev.off()
pdf

# jackstraw（）
# calculate different score for different PCA
# questions I didn't get here . Need to review PCA algorithm
# check how many PCs need to be used for analysis
pbmc <- JackStraw(pbmc, num.replicate = 100)
pbmc <- ScoreJackStraw(pbmc, dims = 1:20)
> pdf("/project/meissner_training/qiongyu/d
> pdf("/project/meissner_training/qiongyu/data/seraut/jackstraw.pdf")
> JackStrawPlot(pbmc, dims = 1:15)
^[[AWarning message:
Removed 23412 rows containing missing values (geom_point).
> dev.off()
pdf
  2
ElbowPlot(pbmc)

# clustering and find the neighbours
> pbmc <- FindNeighbors(pbmc, dims = 1:10)
Computing nearest neighbor graph
Computing SNN
> pbmc<-FindClusters(pbmc,resolution=0.5)
Modularity Optimizer version 1.3.0 by Ludo Waltman and Nees Jan van Eck

Number of nodes: 2638
Number of edges: 95930

Running Louvain algorithm...
0%   10   20   30   40   50   60   70   80   90   100%
[----|----|----|----|----|----|----|----|----|----|
**************************************************|
Maximum modularity in 10 random starts: 0.8737
Number of communities: 9
Elapsed time: 0 seconds
#check the cell group
head(Idents(pbmc), 5)

# run umap
pbmc <- RunUMAP(pbmc, dims = 1:10)
# draw umap
> pdf("/project/meissner_training/qiongyu/data/seraut/umap.pdf")
>  DimPlot(pbmc, reduction = "umap")
> dev.off()

# differential expression
# find marker genes in cluster1
cluster1.markers<-FindMarkers(pbmc, ident.1 = 1, min.pct = 0.25)

# find all markers distinguishing cluster 5 from clusters 0 and 3
cluster5.markers <- FindMarkers(pbmc, ident.1 = 5, ident.2 = c(0, 3), min.pct = 0.25)
head(cluster5.markers, n = 5)

# find upregulated genes in each group
pbmc.markers <- FindAllMarkers(pbmc, only.pos = TRUE, min.pct = 0.25, logfc.threshold = 0.25)
# group those genes by group and only show the most variable genes in each group
pbmc.markers %>% group_by(cluster) %>% top_n(n = 2, wt = avg_logFC)
# weight by avf_logFC
# differnet stastics test can be used in find markers
#need read details





